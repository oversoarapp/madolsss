package com.oversoar.madolsss.Adapter

import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.jude.rollviewpager.adapter.StaticPagerAdapter
import com.oversoar.madolsss.model.GetAdsList
import com.oversoar.madolsss.remote.ApiResult
import com.squareup.picasso.Picasso

class HotPostsImgAdapter(val dataList:MutableList<String>,val readClick:(Int)->Unit ): StaticPagerAdapter() {
    override fun getView(container: ViewGroup?, position: Int): View {
        val view = ImageView(container!!.context)
        if (dataList[position].isNotEmpty()) {
            Picasso.get().load(dataList[position]).fit().into(view)
        }
        view.scaleType = ImageView.ScaleType.CENTER_CROP
        view.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        view.setOnClickListener {
            readClick(position)
        }

        return view
    }

    override fun getCount(): Int {
        return dataList.size
    }
}