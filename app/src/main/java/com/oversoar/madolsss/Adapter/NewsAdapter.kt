package com.oversoar.madolsss.Adapter

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oversoar.madolsss.Commom
import com.oversoar.madolsss.LogInActivity
import com.oversoar.madolsss.R
import com.oversoar.madolsss.model.CommomListModel
import com.squareup.picasso.Picasso


class NewsAdapter(val context: Context, dataList: List<CommomListModel>?, val click:(Int)->Unit, val clickHeart:(Int)->Unit): RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    private var dataList = dataList

    fun updateData(dataList:List<CommomListModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_heart_msg_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val animation = AnimationUtils.loadAnimation(context,R.anim.rv_item_alpha)
        holder.itemView.animation = animation

        if (dataList != null) {

            holder.content.text = dataList!![position].content ?: ""

            if (dataList!![position].photo?.isNotEmpty() == true) {
                Picasso.get().load(dataList!![position].photo).fit().into(holder.imageView)
            } else {
                Picasso.get().load(R.drawable.logomini).fit().into(holder.imageView)
            }

            if (dataList!![position].hearted == true) {
                holder.heart.setImageResource(R.drawable.ic_favorite_red_24dp)
            } else {
                holder.heart.setImageResource(R.drawable.ic_favorite_border_24dp)
            }

            holder.itemView.setOnClickListener {
                click(position)
            }

            holder.heart.setOnClickListener {
                if (Commom.userFbId.isNotEmpty()) {
                    if (dataList!![position].hearted == false) {
                        holder.heart.setImageResource(R.drawable.ic_favorite_red_24dp)
                        dataList!![position].hearted = true
                    } else {
                        holder.heart.setImageResource(R.drawable.ic_favorite_border_24dp)
                        dataList!![position].hearted = false
                    }
                    notifyDataSetChanged()
                    clickHeart(position)
                } else {
                    val from =
                        if (context.toString() == "NewsActivity")
                            "NewsActivity" else "FashionActivity"

                    AlertDialog.Builder(context)
                        .setTitle("訊息提示")
                        .setMessage("請先進行登入哦！")
                        .setNegativeButton("確定") {d,w ->
                            val intent = Intent(context,LogInActivity::class.java)
                            intent.putExtra(LogInActivity.FROM,from)
                            context.startActivity(intent)
                        }
                        .setPositiveButton("取消") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }

            holder.msg.setOnClickListener {
                click(position)
            }

        }
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val content: TextView = itemView.findViewById(R.id.content_item_heart_msg_list)
        val imageView: ImageView = itemView.findViewById(R.id.iv_item_heart_msg_list)
        val heart: ImageView = itemView.findViewById(R.id.heart_btn_item_heart_msg_list)
        val msg: ImageView = itemView.findViewById(R.id.msg_item_heart_msg_list)
    }

}