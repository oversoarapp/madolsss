package com.oversoar.madolsss.Adapter

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.oversoar.madolsss.Commom
import com.oversoar.madolsss.LogInActivity
import com.oversoar.madolsss.R
import com.oversoar.madolsss.model.GetFansPostList
import com.squareup.picasso.Picasso

class GridViewAdapter(
    context: Context,
    dataList: List<GetFansPostList>?,
    val click: ((Int) -> Unit)?,
    val clickHeart:(Int)->Unit
) : BaseAdapter() {

    private val context = context
    private val mLayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var dataList = dataList

    fun updateData(dataList: List<GetFansPostList>) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return dataList?.size?:0
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {



        val v: View = mLayoutInflater.inflate(R.layout.item_vertical, parent, false)
        val tv = v.findViewById<TextView>(R.id.content_item_vertical)
        val imageView = v.findViewById<ImageView>(R.id.iv_item_vertical)
        val heart = v.findViewById<ImageView>(R.id.heart_btn_item_vertical)
        val msg = v.findViewById<ImageView>(R.id.msg_item_vertical)

        val animation = AnimationUtils.loadAnimation(context,R.anim.rv_item_alpha)
        v.animation = animation

        if (dataList!![position].photo?.isNotEmpty() == true) {
            Picasso.get().load(dataList!![position].photo).centerInside().fit().into(imageView)
        } else {
            Picasso.get().load(R.drawable.logomini).fit().into(imageView)
        }

        if (dataList!![position].hearted == true) {
            heart.setImageResource(R.drawable.ic_favorite_red_24dp)
        } else {
            heart.setImageResource(R.drawable.ic_favorite_border_24dp)
        }

        tv.text = dataList!![position].content

        heart.setOnClickListener {
            if (Commom.userFbId.isNotEmpty()) {
                if (dataList!![position].hearted == false) {
                    heart.setImageResource(R.drawable.ic_favorite_red_24dp)
                    dataList!![position].hearted = true
                } else {
                    heart.setImageResource(R.drawable.ic_favorite_border_24dp)
                    dataList!![position].hearted = false
                }
                notifyDataSetChanged()
                clickHeart(position)
            } else {
                AlertDialog.Builder(context)
                    .setTitle("訊息提示")
                    .setMessage("請先進行登入哦！")
                    .setNegativeButton("確定") {d,w ->
                        val intent = Intent(context, LogInActivity::class.java)
                        intent.putExtra(LogInActivity.FROM,"FansPageActivity")
                        context.startActivity(intent)
                    }
                    .setPositiveButton("取消") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        v.setOnClickListener {
            click!!(position)
        }

        msg.setOnClickListener {
            click!!(position)
        }

        return v
    }

    override fun getItem(position: Int): Any? {

        if (dataList != null) {
            return dataList!![position]
        }
        return null
    }

}