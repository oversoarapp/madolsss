package com.oversoar.madolsss.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oversoar.madolsss.R
import com.oversoar.madolsss.model.ArticleModel


class MixArticleAdapter(dataList: List<ArticleModel>?, val click:(Int)->Unit ): RecyclerView.Adapter<MixArticleAdapter.ViewHolder>() {

    private var dataList = dataList

    fun updateData(dataList:List<ArticleModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.right.text = dataList!![position].content?:""
        holder.right.isSingleLine = false
        holder.divider.visibility = View.GONE
        holder.left.visibility = View.GONE

        holder.itemView.setOnClickListener {
            click(position)
        }
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val right: TextView = itemView.findViewById(R.id.right_tv_item_list)
        val divider: View = itemView.findViewById(R.id.divider5)
        val left: TextView = itemView.findViewById(R.id.left_tv_tem_list)
    }
}