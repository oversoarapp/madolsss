package com.oversoar.madolsss.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oversoar.madolsss.R
import com.oversoar.madolsss.model.GetAnnounceList


class AnnouncementAdapter(dataList: List<GetAnnounceList>?,val click:(Int)->Unit ): RecyclerView.Adapter<AnnouncementAdapter.ViewHolder>() {

    private var dataList = dataList

    fun updateData(dataList:List<GetAnnounceList>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = dataList!![position].title?:""
        holder.date.text = dataList!![position].date?:""

        holder.itemView.setOnClickListener {
            click(position)
        }

    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.right_tv_item_list)
        val date: TextView = itemView.findViewById(R.id.left_tv_tem_list)
    }
}