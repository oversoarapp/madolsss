package com.oversoar.madolsss

import com.facebook.AccessToken
import okhttp3.*
import java.util.concurrent.TimeUnit

class GetFbDataApi (private val id:String,private val token: AccessToken) {

    val getAllUserClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun getFbData (callback: Callback){

        val request = Request.Builder()
            .get()
            .url("https://graph.facebook.com/$id/accounts?access_token=$token")
            .build()

        val call: Call = getAllUserClient.newCall(request)

        call.enqueue(callback)
    }

}