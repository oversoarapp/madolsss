package com.oversoar.madolsss

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.oversoar.madolsss.model.GetMessage
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

object Commom {

    var fcmToken = ""
    var userFbId = ""

    fun readPostNgetMsg (context: Context, kind:String, img:String, id:String, content:String, link:String ,videoLink:String, progressBar:ProgressBar) {

        CoroutineScope(Dispatchers.Main).launch {
            var messageResult: ApiResult<List<GetMessage>>
            withContext(Dispatchers.IO) {
                messageResult = getMessageResponse(kind,id)
                ApiClient().createService(ApiService::class.java).read(userFbId,kind,id)
            }

            progressBar.visibility = View.GONE

            when (messageResult) {
                is ApiResult.Success -> {
                    CustomDialog(context,kind,img,link,videoLink, content, id, (messageResult as ApiResult.Success<List<GetMessage>>).data).show()
                    Log.i(
                        context.toString(),
                        "getMessage:${(messageResult as ApiResult.Success<List<GetMessage>>).data}"
                    )
                }
                is ApiResult.Error -> {
                    Log.e(
                        context.toString(),
                        "getMessageError: ${(messageResult as ApiResult.Error).exception}"
                    )
                }
            }

        }
    }

    private suspend fun getMessageResponse(kind:String,kid:String): ApiResult<List<GetMessage>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getMessage(kind,kid)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }
}