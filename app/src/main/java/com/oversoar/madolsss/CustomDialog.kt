package com.oversoar.madolsss

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oversoar.madolsss.Commom.userFbId
import com.oversoar.madolsss.model.GetMessage
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class CustomDialog(private val _context: Context, private val kind: String, private val img:String,private val link:String ,private val videoLink:String, private val content:String, private val id:String, private val messageList:List<GetMessage>?) : Dialog(_context) {

    private val TAG = "CustomDialog"
    private var nAdapter: MsgAdapter? = null
    private lateinit var et:EditText

    init {
        setCancelable(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.custom_dialog)

        window?.setLayout(1000, WindowManager.LayoutParams.WRAP_CONTENT)

        val tv = findViewById<TextView>(R.id.content_custom_dialog)
        val iv = findViewById<ImageView>(R.id.iv_custom_dialog)
        et = findViewById<EditText>(R.id.msg_et_custom_dialog)
        val rv = findViewById<RecyclerView>(R.id.rv_custom_dialog)
        val send = findViewById<TextView>(R.id.send_custom_dialog)
        val linkTv = findViewById<TextView>(R.id.video_link_custom_dialog)

        tv.text = "$content\nsource: $link"
        nAdapter = MsgAdapter(messageList)
        rv.adapter = nAdapter

        if (kind.isEmpty()) {
            send.visibility = View.GONE
            et.visibility = View.GONE
        }

        if (videoLink.isNotEmpty()) {
            linkTv.text = videoLink
        } else {
            linkTv.visibility = View.GONE
        }

        if (img.isNotEmpty()) {
            Picasso.get().load(img).fit().into(iv)
        } else {
            Picasso.get().load(R.drawable.logomini).fit().into(iv)
        }

        linkTv.setOnClickListener {
            val uri = Uri.parse(videoLink) //要跳轉的網址
            val intent = Intent(Intent.ACTION_VIEW, uri)
            _context.startActivity(intent)
        }

        send.setOnClickListener {
            if (et.text.isNotEmpty()) {
                if (userFbId.isNotEmpty()) {
                    insertMsg(et.text.toString())
                } else {
                    var from = ""
                    var strContext = _context.toString()

                    when {
                        strContext.contains("MainActivity") -> from = "MainActivity"
                        strContext.contains("FansPageActivity") -> from = "FansPageActivity"
                        strContext.contains("HostPageActivity") -> from = "HostPageActivity"
                        strContext.contains("NewsActivity") -> from = "NewsActivity"
                        strContext.contains("FashionActivity") -> from = "FashionActivity"
                    }

                    AlertDialog.Builder(_context)
                        .setTitle("訊息提示")
                        .setMessage("請先進行登入哦！")
                        .setNegativeButton("確定") { d, w ->
                            val intent = Intent(_context, LogInActivity::class.java)
                            intent.putExtra(LogInActivity.FROM, from)
                            intent.putExtra(LogInActivity.FINISH, true)
                            _context.startActivity(intent)
                        }
                        .setPositiveButton("取消") { d, w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }

    private fun insertMsg(msg:String) {

        progressBar3.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var messageResult: ApiResult<List<GetMessage>>
            withContext(Dispatchers.IO) {
                messageResult = insertMessageResponse(userFbId,kind,id,msg)
            }

            progressBar3.visibility = View.GONE

            when (messageResult) {
                is ApiResult.Success -> {
                    nAdapter?.updateData((messageResult as ApiResult.Success<List<GetMessage>>).data)
                    et.setText("")
                    Log.i(
                        TAG,
                        "getMessage:${(messageResult as ApiResult.Success<List<GetMessage>>).data}"
                    )
                }
                is ApiResult.Error -> {
                    Log.e(
                        TAG,
                        "getMessageError: ${(messageResult as ApiResult.Error).exception}"
                    )
                }
            }
        }
    }

    private suspend fun insertMessageResponse(user:String,kind:String,kid:String,content:String): ApiResult<List<GetMessage>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).insertMessage(user,kind,kid,content)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    class MsgAdapter(private var dataList:List<GetMessage>?): RecyclerView.Adapter<MsgAdapter.ViewHolder>() {

        fun updateData(dataList: List<GetMessage>){
            this.dataList = dataList
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MsgAdapter.ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return dataList?.size?:0
        }

        override fun onBindViewHolder(holder: MsgAdapter.ViewHolder, position: Int) {
            if (dataList != null) {
                holder.title.text = dataList!![position].user_name
                holder.content.text = dataList!![position].content
            }
        }

        inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
            val title: TextView = itemView.findViewById(R.id.left_tv_tem_list)
            val content: TextView = itemView.findViewById(R.id.right_tv_item_list)
        }

    }
}