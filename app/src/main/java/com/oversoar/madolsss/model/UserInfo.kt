package com.oversoar.madolsss.model

data class UserInfo(
    val created_at: String,
    val email: String,
    val email_verified_at: Any,
    val fbid: String,
    val id: Int,
    val name: String,
    val password: String,
    val remember_token: Any,
    val updated_at: String
)