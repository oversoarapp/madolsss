package com.oversoar.madolsss.model

data class GetAdsList(
    val created_at: String? = null,
    val date: String? = null,
    val id: Int? = null,
    val link: String? = null,
    val photo: String? = null,
    val updated_at: String? = null
)