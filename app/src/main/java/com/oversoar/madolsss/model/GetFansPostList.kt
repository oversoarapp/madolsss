package com.oversoar.madolsss.model

data class GetFansPostList(
    val Click_through_rate: String? = null,
    val content: String? = null,
    val created_at: String? = null,
    val date: String? = null,
    val display: String? = null,
    val fb_post_id: String? = null,
    val id: String? = null,
    val link: String? = null,
    val onlineDate: String? = null,
    val photo: String? = null,
    val updated_at: String? = null,
    val viedo_link: String? = null,
    var hearted:Boolean? = null
)