package com.oversoar.madolsss.model

data class CommomListModel(
    val content: String? = null,
    val created_at: String? = null,
    val date: String? = null,
    val fb_post_id: String? = null,
    val id: String? = null,
    val link: String? = null,
    val photo: String? = null,
    val updated_at: String? = null,
    val viedo_link: String? = null,
    var hearted:Boolean? = null
)