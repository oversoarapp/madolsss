package com.oversoar.madolsss.model

data class GetMessage(
    val content: String? = null,
    val created_at: String? = null,
    val hid: Int? = null,
    val kid: Int? = null,
    val kind: String? = null,
    val user: String? = null,
    val user_name: String? = null
)