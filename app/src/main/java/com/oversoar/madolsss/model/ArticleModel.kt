package com.oversoar.madolsss.model

data class ArticleModel(
    val content: String? = null,
    val fb_post_id: String? = null,
    val id: String? = null,
    val kind: String? = null,
    val link: String? = null,
    val photo: String? = null,
    val viedo_link: String? = null,
    val created_at:String? = null
)