package com.oversoar.madolsss.model

data class GetAllList(
    val adsdata: List<GetAdsList>,
    val announcedata: List<GetAnnounceList>,
    val articlesdata: List<GetHostPostList>,
    val newsdata: List<CommomListModel>,
    val postsdata: List<GetFansPostList>,
    val mixdata: List<ArticleModel>
)