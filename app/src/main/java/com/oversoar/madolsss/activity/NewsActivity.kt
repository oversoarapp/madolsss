package com.oversoar.madolsss

import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.oversoar.madolsss.Adapter.AdsAdapter
import com.oversoar.madolsss.Adapter.NewsAdapter
import com.oversoar.madolsss.Commom.readPostNgetMsg
import com.oversoar.madolsss.Commom.userFbId
import com.oversoar.madolsss.activity.FashionActivity
import com.oversoar.madolsss.model.GetAdsList
import com.oversoar.madolsss.model.CommomListModel
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import kotlinx.android.synthetic.main.activity_news.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class NewsActivity: AppCompatActivity() {

    private val TAG = "NewsActivity"
    private var nAdapter: NewsAdapter? = null
    private var newsList: List<CommomListModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        initView()
        getData()

    }

    private fun initView() {

        news_btn_news.setTextColor(getColor(R.color.colorBlack))

        val click:(Int) -> Unit = {

            progressBar2.visibility = View.VISIBLE

            readPostNgetMsg(this,
                "3",
                newsList!![it].photo?:"",
                newsList!![it].id?:"",
                newsList!![it].content?:"",
                newsList!![it].link?:"",
                newsList!![it].viedo_link?:"",
                progressBar2)

        }

        val clickHeart:(Int) -> Unit = {
            var news = newsList!![it]
            CoroutineScope(Dispatchers.IO).launch {
                ApiClient().createService(ApiService::class.java)
                    .clickHeart("3", news.id ?: "", userFbId)
            }
        }

        setAdapter(click,clickHeart)

        home_btn_news.setOnClickListener {
            nextPage(MainActivity::class.java)
        }

        fans_page_btn_news.setOnClickListener {
            nextPage(FansPageActivity::class.java)
        }

        host_page_btn_news.setOnClickListener {
            nextPage(HostPageActivity::class.java)
        }

        fashion_btn_news.setOnClickListener {
            nextPage(FashionActivity::class.java)
        }

    }

    private fun nextPage(_class:Class<*>) {
        val intent = Intent(this,_class)
        val option = ActivityOptions.makeSceneTransitionAnimation(this)
        startActivity(intent,option.toBundle())
    }

    private fun getData() {
        CoroutineScope(Dispatchers.Main).launch {
            var newsResult: ApiResult<List<CommomListModel>>
            var adsResult: ApiResult<List<GetAdsList>>
            withContext(Dispatchers.IO) {
                newsResult = getNewsListResponse(userFbId)
                adsResult = getAdsListResponse()
            }
            progressBar2.visibility = View.GONE
            when (newsResult) {
                is ApiResult.Success -> {
                    newsList = (newsResult as ApiResult.Success<List<CommomListModel>>).data
                    nAdapter?.updateData((newsResult as ApiResult.Success<List<CommomListModel>>).data)
                    Log.i(TAG,"getNewsList:${(newsResult as ApiResult.Success<List<CommomListModel>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getNewsListError: ${(newsResult as ApiResult.Error).exception}")
                }
            }

            when (adsResult) {
                is ApiResult.Success -> {
                    val photoList = mutableListOf<String>()
                    (adsResult as ApiResult.Success<List<GetAdsList>>).data.forEach {
                        if(it.photo?.isNotEmpty() == true) {
                            photoList.add(it.photo)
                        }
                    }
                    rollView_news.setAdapter(AdsAdapter(photoList))
                    Log.i(TAG,"getAdsList:${(adsResult as ApiResult.Success<List<GetAdsList>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getAdsListError: ${(adsResult as ApiResult.Error).exception}")
                }
            }

        }
    }

    private fun setAdapter(click:(Int) -> Unit,clickHeart:(Int) -> Unit) {
        nAdapter = NewsAdapter(this,null,click,clickHeart)
        rv_news.adapter = nAdapter
//        val defaultItemAnimator = DefaultItemAnimator()
//        defaultItemAnimator.addDuration = 1000
//        defaultItemAnimator.removeDuration = 1000
//        rv_news.itemAnimator = defaultItemAnimator
    }

    private fun refreshNewsResponse() {
        progressBar2.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var newsResult: ApiResult<List<CommomListModel>>
            withContext(Dispatchers.IO) {
                newsResult = getNewsListResponse(userFbId)
            }
            progressBar2.visibility = View.GONE
            when (newsResult) {
                is ApiResult.Success -> {
                    newsList = (newsResult as ApiResult.Success<List<CommomListModel>>).data
                    nAdapter?.updateData((newsResult as ApiResult.Success<List<CommomListModel>>).data)
                    Log.i(TAG,"getNewsList:${(newsResult as ApiResult.Success<List<CommomListModel>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getNewsListError: ${(newsResult as ApiResult.Error).exception}")
                }
            }
        }
    }

    private suspend fun getNewsListResponse(user:String): ApiResult<List<CommomListModel>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getNewsList(user)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getAdsListResponse(): ApiResult<List<GetAdsList>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getAdsList()
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

}