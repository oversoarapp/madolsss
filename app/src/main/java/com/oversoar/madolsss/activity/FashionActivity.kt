package com.oversoar.madolsss.activity

import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.oversoar.madolsss.*
import com.oversoar.madolsss.Adapter.AdsAdapter
import com.oversoar.madolsss.Adapter.NewsAdapter
import com.oversoar.madolsss.model.CommomListModel
import com.oversoar.madolsss.model.GetAdsList
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import kotlinx.android.synthetic.main.activity_news.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FashionActivity : AppCompatActivity() {

    private val TAG = "FashionActivity"
    private var nAdapter: NewsAdapter? = null
    private var dataList: List<CommomListModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        initView()
        getData()

    }

    private fun initView() {

        fashion_btn_news.setTextColor(getColor(R.color.colorBlack))

        val click:(Int) -> Unit = {

            progressBar2.visibility = View.VISIBLE

            Commom.readPostNgetMsg(
                this,
                "4",
                dataList!![it].photo ?: "",
                dataList!![it].id ?: "",
                dataList!![it].content ?: "",
                dataList!![it].link ?: "",
                dataList!![it].viedo_link ?: "",
                progressBar2
            )

        }

        val clickHeart:(Int) -> Unit = {
            if (Commom.userFbId.isNotEmpty()) {
                var data = dataList!![it]
                CoroutineScope(Dispatchers.IO).launch {
                    ApiClient().createService(ApiService::class.java)
                        .clickHeart("4", data.id ?: "", Commom.userFbId)
                }
            } else {
                AlertDialog.Builder(this)
                    .setTitle("訊息提示")
                    .setMessage("請先進行登入哦！")
                    .setPositiveButton("確定") {d,w ->
                        startActivity(Intent(this, LogInActivity::class.java))
                        //TODO 重撈newsList
                    }
                    .setNegativeButton("取消") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        nAdapter = NewsAdapter(this,null,click,clickHeart)
        rv_news.adapter = nAdapter

        home_btn_news.setOnClickListener {
            nextPage(MainActivity::class.java)
        }

        fans_page_btn_news.setOnClickListener {
            nextPage(FansPageActivity::class.java)
        }

        host_page_btn_news.setOnClickListener {
            nextPage(HostPageActivity::class.java)
        }

        news_btn_news.setOnClickListener {
            nextPage(NewsActivity::class.java)
        }

    }

    private fun nextPage(_class:Class<*>) {
        val intent = Intent(this,_class)
        val option = ActivityOptions.makeSceneTransitionAnimation(this)
        startActivity(intent,option.toBundle())
    }

    private fun getData() {
        CoroutineScope(Dispatchers.Main).launch {
            var result: ApiResult<List<CommomListModel>>
            var adsResult: ApiResult<List<GetAdsList>>
            withContext(Dispatchers.IO) {
                result = getFashionListResponse(Commom.userFbId)
                adsResult = getAdsListResponse()
            }
            progressBar2.visibility = View.GONE
            when (result) {
                is ApiResult.Success -> {
                    dataList = (result as ApiResult.Success<List<CommomListModel>>).data
                    nAdapter?.updateData((result as ApiResult.Success<List<CommomListModel>>).data)
                    Log.i(TAG,"getFashionList:${(result as ApiResult.Success<List<CommomListModel>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getFashionListError: ${(result as ApiResult.Error).exception}")
                }
            }

            when (adsResult) {
                is ApiResult.Success -> {
                    val photoList = mutableListOf<String>()
                    (adsResult as ApiResult.Success<List<GetAdsList>>).data.forEach {
                        if(it.photo?.isNotEmpty() == true) {
                            photoList.add(it.photo)
                        }
                    }
                    rollView_news.setAdapter(AdsAdapter(photoList))
                    Log.i(TAG,"getAdsList:${(adsResult as ApiResult.Success<List<GetAdsList>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getAdsListError: ${(adsResult as ApiResult.Error).exception}")
                }
            }

        }
    }

    private suspend fun getFashionListResponse(user:String): ApiResult<List<CommomListModel>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getFashionList(user)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getAdsListResponse(): ApiResult<List<GetAdsList>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getAdsList()
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

}