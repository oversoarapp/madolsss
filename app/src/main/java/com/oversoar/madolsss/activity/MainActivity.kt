package com.oversoar.madolsss

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.facebook.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.oversoar.madolsss.Adapter.AdsAdapter
import com.oversoar.madolsss.Adapter.AnnouncementAdapter
import com.oversoar.madolsss.Adapter.MixArticleAdapter
import com.oversoar.madolsss.Adapter.HotPostsImgAdapter
import com.oversoar.madolsss.Commom.fcmToken
import com.oversoar.madolsss.Commom.userFbId
import com.oversoar.madolsss.activity.FashionActivity
import com.oversoar.madolsss.model.GetAllList
import com.oversoar.madolsss.model.ArticleModel
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import kotlinx.android.synthetic.main.s.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private var isLoggedIn = false
    private val FB_LOGIN_REQUESTCODE = 64206
    private var callbackManager: CallbackManager? = null

    override fun onResume() {
        super.onResume()

        isLoggedIn = userFbId.isEmpty() == false

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.s)

        getToken()
        getAllList()
        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            FB_LOGIN_REQUESTCODE -> {
                callbackManager?.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun initView() {
        home_btn_main.setTextColor(getColor(R.color.colorBlack))

        post_btn_main.setOnClickListener {

            if (isLoggedIn) {
                startActivity(Intent(this,PostActivity::class.java))
            } else {
                val intent = Intent(this,LogInActivity::class.java)
                intent.putExtra(LogInActivity.FROM,"MainActivity")
                startActivity(intent)
            }

        }

        news_btn_main.setOnClickListener {
            nextPage(NewsActivity::class.java)
        }

        fans_page_btn_main.setOnClickListener {
            nextPage(FansPageActivity::class.java)
        }

        host_page_btn_main.setOnClickListener {
            nextPage(HostPageActivity::class.java)
        }

        fashion_btn_main.setOnClickListener {
            nextPage(FashionActivity::class.java)
        }

    }
    
    private fun nextPage(_class:Class<*>) {
        val intent = Intent(this,_class)
        val option = ActivityOptions.makeSceneTransitionAnimation(this)
        startActivity(intent,option.toBundle())
    }

    private fun getToken() {

        FirebaseMessaging.getInstance().subscribeToTopic("global")
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            this@MainActivity,
            OnSuccessListener<InstanceIdResult> { instanceIdResult ->
                fcmToken = instanceIdResult.token
                Log.i("FCMToken", fcmToken)
            })
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }

    private fun getAllList() {
        CoroutineScope(Dispatchers.Main).launch {
            var result: ApiResult<GetAllList>
            var hotPostResult: ApiResult<List<ArticleModel>>
            withContext(Dispatchers.IO) {
                result = getAllListResponse()
                hotPostResult = getHotPostsResponse()
            }

            progressBar.visibility = View.GONE

            when (result) {
                is ApiResult.Success -> {
                    setAdapter((result as ApiResult.Success<GetAllList>).data)
                    setHotPostsAdapter((result as ApiResult.Success<GetAllList>).data.mixdata)

                    Log.i("MainActivity","getAllList:${(result as ApiResult.Success<GetAllList>).data}")
                }
                is ApiResult.Error -> {
                    Log.e("MainActivity", "getAllListError: ${(result as ApiResult.Error).exception}")
                }
            }

//            when(hotPostResult) {
//                is ApiResult.Success -> {
//                    setHotPostsAdapter((hotPostResult as ApiResult.Success<List<ArticleModel>>).data)
//                    Log.i("MainActivity","getHotPostsList:${(hotPostResult as ApiResult.Success<List<ArticleModel>>).data}")
//                }
//                is ApiResult.Error -> {
//                    Log.e("MainActivity", "getHotPostsError: ${(hotPostResult as ApiResult.Error).exception}")
//                }
//            }

        }
    }

    private fun setAdapter(dataList:GetAllList) {

        val annoClick:(Int)->Unit = {
            CustomDialog(
                this,
                "",
                dataList.announcedata[it].photo ?: "",
                dataList.announcedata[it].link ?: "",
                dataList.announcedata[it].viedo_link ?: "",
                dataList.announcedata[it].content?:"",
                dataList.announcedata[it].id?:"",
                null
            ).show()
        }

        rv1_main.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
        rv1_main.adapter = AnnouncementAdapter(dataList.announcedata,annoClick)
        val photoList = mutableListOf<String>()
        dataList.adsdata.forEach {
            if(it.photo?.isNotEmpty() == true) {
                photoList.add(it.photo)
            }
        }

        rollView_main.setAdapter(AdsAdapter(photoList))
    }

    private fun setHotPostsAdapter(dataList:List<ArticleModel>) {

        val click:(Int)->Unit = {

            progressBar.visibility = View.VISIBLE

            Commom.readPostNgetMsg(
                this,
                dataList[it].kind?:"" ,
                dataList[it].photo ?: "",
                dataList[it].id ?: "",
                dataList[it].content ?: "",
                dataList[it].link?:"",
                dataList[it].viedo_link?:"",
                progressBar
            )
        }

        val hotPostsClick:(Int)->Unit = {

            progressBar.visibility = View.VISIBLE

            Commom.readPostNgetMsg(
                this,
                dataList[it].kind?:"" ,
                dataList[it].photo ?: "",
                dataList[it].id ?: "",
                dataList[it].content ?: "",
                dataList[it].link?:"",
                dataList[it].viedo_link?:"",
                progressBar
            )
        }

        rv2_main.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
        rv2_main.adapter = MixArticleAdapter(dataList,hotPostsClick)
        val photoList = mutableListOf<String>()
        dataList.forEach {
            photoList.add(it.photo?:"")
        }
        rollView_hot_main.setAdapter(HotPostsImgAdapter(photoList,click))
    }

    private suspend fun getAllListResponse(): ApiResult<GetAllList> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getAllList()
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getHotPostsResponse(): ApiResult<List<ArticleModel>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getHotPosts()
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

}
