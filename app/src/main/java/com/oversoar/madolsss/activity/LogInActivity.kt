package com.oversoar.madolsss

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.Login
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.oversoar.madolsss.Commom.userFbId
import com.oversoar.madolsss.activity.FashionActivity
import com.oversoar.madolsss.model.UserInfo
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LogInActivity: AppCompatActivity() {

    companion object{
        val FROM = "from"
        val FINISH = "finish"
    }

    private val TAG = "LoginActivity"
    private var from = ""
    private var finish = false
    private var callbackManager: CallbackManager? = null
    private val FB_LOGIN_REQUESTCODE = 64206

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        fbLogin()
        from = intent.getStringExtra(FROM)?:""
        finish = intent.getBooleanExtra(FINISH,false)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            FB_LOGIN_REQUESTCODE -> {
                callbackManager?.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun fbLogin() {

        val loginManager = LoginManager.getInstance()
        callbackManager = CallbackManager.Factory.create()
        loginManager.loginBehavior = LoginBehavior.NATIVE_WITH_FALLBACK
        val permission = mutableSetOf<String>("email", "public_profile")
        loginManager.logInWithReadPermissions(this, permission)
        loginManager.registerCallback(
            callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: com.facebook.login.LoginResult) {
                    val request =
                        GraphRequest.newMeRequest(loginResult.accessToken) { result, response ->
                            try {
                                if (result.has("name")) {
                                    val name = result.getString("name")
                                    val email = result.getString("email")
                                    val id = result.getString("id")
                                    login(id, name, email, Commom.fcmToken)
                                }
                                Log.i("FBLOGIN_JSON_RES", result.toString())
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                    val parameters = Bundle()
                    parameters.putString("fields", "name,email,id,picture.type(large)")
                    request.parameters = parameters
                    request.executeAsync()

                }

                override fun onCancel() {
                    finish()
                }

                override fun onError(error: FacebookException?) {
                    Toast.makeText(this@LogInActivity, "SOMETHING WRONG", Toast.LENGTH_SHORT).show()
                    finish()
                    Log.e("FBLOGIN-ERROR", error.toString())
                }
            })
    }

    private fun login(fbId: String, name: String, email: String, token: String) {
        CoroutineScope(Dispatchers.Main).launch {
            var result: ApiResult<UserInfo>

            withContext(Dispatchers.IO) {
                result = loginResponse(fbId, name, email, token)
            }

            when (result) {
                is ApiResult.Success -> {
                    Log.i(TAG, (result as ApiResult.Success<UserInfo>).data.toString())
                }
                is ApiResult.Error -> {
                    Log.i(TAG, (result as ApiResult.Error).exception.toString())
                }
            }

            val sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
            sp.edit().putString("fbid",fbId).apply()
            userFbId = fbId

            if (finish) {
                finish()
            } else {
                val _class = when (from) {
                    "MainActivity" -> MainActivity::class.java
                    "NewsActivity" -> NewsActivity::class.java
                    "FansPageActivity" -> FansPageActivity::class.java
                    "HostPageActivity" -> HostPageActivity::class.java
                    "FashionActivity" -> FashionActivity::class.java
                    else -> MainActivity::class.java
                }
                startActivity(Intent(this@LogInActivity,_class))
                finish()
            }

        }
    }

    private suspend fun loginResponse(fbId:String, name:String, email:String, token:String): ApiResult<UserInfo>{
        return try {
            val result = ApiClient().createService(ApiService::class.java).login(fbId, name, email, token)
            ApiResult.Success(result)
        }catch (t:Throwable) {
            ApiResult.Error(t)
        }
    }
}