package com.oversoar.madolsss

import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.oversoar.madolsss.Adapter.AdsAdapter
import com.oversoar.madolsss.Adapter.GridViewAdapter
import com.oversoar.madolsss.Commom.userFbId
import com.oversoar.madolsss.model.GetAdsList
import com.oversoar.madolsss.model.GetFansPostList
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import kotlinx.android.synthetic.main.activity_fans_page.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FansPageActivity : AppCompatActivity() {

    private val TAG = "FansPage"
    private var nAdapter: GridViewAdapter? = null
    private var dataList: List<GetFansPostList>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fans_page)

        getData()
        initView()

    }

    private fun initView() {

        fans_page_btn.setTextColor(getColor(R.color.colorBlack))

        val click:(Int)->Unit = {

            progressBar2.visibility = View.VISIBLE

            Commom.readPostNgetMsg(
                this,
                "2",
                dataList!![it].photo ?: "",
                dataList!![it].id ?: "",
                dataList!![it].content ?: "",
                dataList!![it].link?:"",
                dataList!![it].viedo_link?:"",
                progressBar2
            )

        }

        val clickHeart:(Int) -> Unit = {
            var fansPost = dataList!![it]
            CoroutineScope(Dispatchers.IO).launch {
                ApiClient().createService(ApiService::class.java)
                    .clickHeart("2", fansPost.id ?: "", userFbId)
            }
        }

        nAdapter = GridViewAdapter(this,null,click,clickHeart)
        gv_fans_page.adapter = nAdapter

        news_page_btn_fans_page.setOnClickListener {
            val intent = Intent(this,NewsActivity::class.java)
            val option = ActivityOptions.makeSceneTransitionAnimation(this)
            startActivity(intent,option.toBundle())
        }

        home_btn_fans_page.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            val option = ActivityOptions.makeSceneTransitionAnimation(this)
            startActivity(intent,option.toBundle())
        }

        host_page_btn_fans_page.setOnClickListener {
            val intent = Intent(this,HostPageActivity::class.java)
            val option = ActivityOptions.makeSceneTransitionAnimation(this)
            startActivity(intent,option.toBundle())
        }

    }

    private fun getData() {
        CoroutineScope(Dispatchers.Main).launch {
            var adsResult: ApiResult<List<GetAdsList>>
            var fansPostResult: ApiResult<List<GetFansPostList>>

            withContext(Dispatchers.IO) {
                adsResult = getAdsListResponse()
                fansPostResult = getFansPostResponse()
            }

            progressBar2.visibility = View.GONE

            when (adsResult) {
                is ApiResult.Success -> {
                    val photoList = mutableListOf<String>()
                    (adsResult as ApiResult.Success<List<GetAdsList>>).data.forEach {
                        if(it.photo?.isNotEmpty() == true) {
                            photoList.add(it.photo)
                        }
                    }
                    rollView_fans_page.setAdapter(AdsAdapter(photoList))
                    Log.i(TAG,"getAdsList:${(adsResult as ApiResult.Success<List<GetAdsList>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getAdsListError: ${(adsResult as ApiResult.Error).exception}")
                }
            }

            when (fansPostResult) {
                is ApiResult.Success -> {
                    dataList = (fansPostResult as ApiResult.Success<List<GetFansPostList>>).data
                    nAdapter?.updateData((fansPostResult as ApiResult.Success<List<GetFansPostList>>).data)
                    Log.i(TAG,"getFansPostList:${(fansPostResult as ApiResult.Success<List<GetFansPostList>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getFansPostListError: ${(fansPostResult as ApiResult.Error).exception}")
                }
            }
        }
    }

    private suspend fun getAdsListResponse(): ApiResult<List<GetAdsList>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getAdsList()
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getFansPostResponse(): ApiResult<List<GetFansPostList>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getFansPostList(userFbId)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

}
