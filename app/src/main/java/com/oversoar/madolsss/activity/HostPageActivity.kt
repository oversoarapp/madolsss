package com.oversoar.madolsss

import android.app.ActivityOptions
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.oversoar.madolsss.Adapter.AdsAdapter
import com.oversoar.madolsss.Adapter.HostGridViewAdapter
import com.oversoar.madolsss.Commom.userFbId
import com.oversoar.madolsss.activity.FashionActivity
import com.oversoar.madolsss.model.GetAdsList
import com.oversoar.madolsss.model.GetHostPostList
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import kotlinx.android.synthetic.main.activity_fans_page.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HostPageActivity : AppCompatActivity() {

    private var nAdapter: HostGridViewAdapter? = null
    private val TAG = "HostPage"
    private var dataList: List<GetHostPostList>? = null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fans_page)

        getData()
        initView()

    }

    private fun initView() {

        host_page_btn_fans_page.setTextColor(getColor(R.color.colorBlack))

        val click:(Int)->Unit = {

            progressBar2.visibility = View.VISIBLE

            Commom.readPostNgetMsg(
                this,
                "1",
                dataList!![it].photo ?: "",
                dataList!![it].id ?: "",
                dataList!![it].content ?: "",
                dataList!![it].link?:"",
                dataList!![it].viedo_link?:"",
                progressBar2
            )

        }

        val clickHeart:(Int) -> Unit = {

            var hostPost = dataList!![it]
            CoroutineScope(Dispatchers.IO).launch {
                ApiClient().createService(ApiService::class.java)
                    .clickHeart("1", hostPost.id ?: "", Commom.userFbId)
            }

        }

        nAdapter = HostGridViewAdapter(this,null,click,clickHeart)
        gv_fans_page.adapter = nAdapter
        
        news_page_btn_fans_page.setOnClickListener {
            nextPage(NewsActivity::class.java)
        }

        home_btn_fans_page.setOnClickListener {
           nextPage(MainActivity::class.java)
        }

        fans_page_btn.setOnClickListener {
            nextPage(FansPageActivity::class.java)
        }

        fashion_btn_fans_page.setOnClickListener {
            nextPage(FashionActivity::class.java)
        }

    }

    private fun nextPage(_class:Class<*>) {
        val intent = Intent(this,_class)
        val option = ActivityOptions.makeSceneTransitionAnimation(this)
        startActivity(intent,option.toBundle())
    }


    private fun getData() {
        CoroutineScope(Dispatchers.Main).launch {
            var adsResult: ApiResult<List<GetAdsList>>
            var hostPostResult: ApiResult<List<GetHostPostList>>
            
            withContext(Dispatchers.IO) {
                adsResult = getAdsListResponse()
                hostPostResult = getHostPostResponse()
            }

            progressBar2.visibility = View.GONE

            when (adsResult) {
                is ApiResult.Success -> {
                    setRollImgAdapter((adsResult as ApiResult.Success<List<GetAdsList>>).data)
                    Log.i(TAG,"getAdsList:${(adsResult as ApiResult.Success<List<GetAdsList>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getAdsListError: ${(adsResult as ApiResult.Error).exception}")
                }
            }

            when (hostPostResult) {
                is ApiResult.Success -> {
                    dataList = (hostPostResult as ApiResult.Success<List<GetHostPostList>>).data
                    nAdapter?.updateData((hostPostResult as ApiResult.Success<List<GetHostPostList>>).data)
                    Log.i(TAG,"getHostPostList:${(hostPostResult as ApiResult.Success<List<GetHostPostList>>).data}")
                }
                is ApiResult.Error -> {
                    Log.e(TAG, "getHostPostListError: ${(hostPostResult as ApiResult.Error).exception}")
                }
            }
        }
    }

    private fun setRollImgAdapter(dataList:List<GetAdsList>) {
        val photoList = mutableListOf<String>()
        dataList.forEach {
            if(it.photo?.isNotEmpty() == true) {
                photoList.add(it.photo)
            }
        }
        rollView_fans_page.setAdapter(AdsAdapter(photoList))
    }

    private suspend fun getAdsListResponse(): ApiResult<List<GetAdsList>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getAdsList()
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getHostPostResponse(): ApiResult<List<GetHostPostList>> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getHostPostList(userFbId)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

}
