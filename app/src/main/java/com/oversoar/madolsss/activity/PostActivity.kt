package com.oversoar.madolsss

import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.oversoar.madolsss.Commom.userFbId
import com.oversoar.madolsss.activity.FashionActivity
import com.oversoar.madolsss.remote.ApiClient
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.madolsss.remote.ApiService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activty_post.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import kotlin.math.max
import kotlin.math.sqrt

class PostActivity: AppCompatActivity() {

    private val SELECT_PHOTO = 123
    private var img64 = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activty_post)

        initView()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SELECT_PHOTO -> {
                if (data?.data != null) {
                    Picasso.get().load(data.data).fit().into(add_img_post)
                    val uri = data.data
                    //取得圖檔的路徑 (media uri)
                    val stream = contentResolver.openInputStream(uri!!)
                    //轉為絕對路徑
                    var bitmap = BitmapFactory.decodeStream(stream)
                    bitmap = compressionBitmap(bitmap, 150) as Bitmap
                    img64 = convertToBase64(bitmap)

                    Log.i("base64", img64)
                }
            }
        }

    }

    private fun initView() {

        add_img_post.setOnClickListener {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, SELECT_PHOTO)
        }

        done_btn_post.setOnClickListener {

            val content = content_et_post.text.toString()
            val link = link_et_post.text.toString()
            val videoLink = video_link_et_post.text.toString()

            if (content.isEmpty()) {
                return@setOnClickListener
            }

            CoroutineScope(Dispatchers.Main).launch {
                progressBar4.visibility = View.VISIBLE
                var result = ""
                withContext(Dispatchers.IO) {
                    result = ApiClient().createService(ApiService::class.java).newPost(userFbId,content,link,videoLink,img64)
                }
                if (result == "SUCCESS") {
                    Toast.makeText(this@PostActivity,"投稿成功！",Toast.LENGTH_SHORT).show()
                    nextPage(MainActivity::class.java)
                    finish()
                }
            }
        }

        home_btn_post.setOnClickListener {
            nextPage(MainActivity::class.java)
        }

        fans_page_btn_post.setOnClickListener {
            nextPage(FansPageActivity::class.java)
        }

        news_page_btn_post.setOnClickListener {
           nextPage(NewsActivity::class.java)
        }

        host_page_btn_post.setOnClickListener {
            nextPage(HostPageActivity::class.java)
        }

        fashion_btn_post.setOnClickListener {
            nextPage(FashionActivity::class.java)
        }

    }

    private fun nextPage(_class:Class<*>) {
        val intent = Intent(this,_class)
        val option = ActivityOptions.makeSceneTransitionAnimation(this)
        startActivity(intent,option.toBundle())
    }

    private fun compressionBitmap (bitmap:Bitmap, maxSize:Long):Bitmap? {
        return try {
            var width = bitmap.width
            var height = bitmap.height
            val maxLength = max(width,height)
            if (maxLength > maxSize) {
                val scale = sqrt(maxLength / maxSize.toDouble())
                width = (width / scale).toInt()
                height = (height / scale).toInt()
            }
            Bitmap.createScaledBitmap(bitmap,width,height,false)
        } catch (e:Throwable) {
            e.printStackTrace()
            null
        }
    }

    private fun convertToBase64 (bitmap:Bitmap):String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

}