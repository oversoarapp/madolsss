package com.oversoar.madolsss.remote

import com.oversoar.madolsss.model.*
import com.oversoar.madolsss.remote.ApiUrl.clickHeart
import com.oversoar.madolsss.remote.ApiUrl.getAdsListUrl
import com.oversoar.madolsss.remote.ApiUrl.getAllListUrl
import com.oversoar.madolsss.remote.ApiUrl.getAnnouncementListUrl
import com.oversoar.madolsss.remote.ApiUrl.getFanPostListUrl
import com.oversoar.madolsss.remote.ApiUrl.getFashionList
import com.oversoar.madolsss.remote.ApiUrl.readUrl
import com.oversoar.madolsss.remote.ApiUrl.getNewsListUrl
import com.oversoar.madolsss.remote.ApiUrl.getHostPostListUrl
import com.oversoar.madolsss.remote.ApiUrl.getHotPosts
import com.oversoar.madolsss.remote.ApiUrl.getMessage
import com.oversoar.madolsss.remote.ApiUrl.hostUrl
import com.oversoar.madolsss.remote.ApiUrl.insertMessage
import com.oversoar.madolsss.remote.ApiUrl.login
import com.oversoar.madolsss.remote.ApiUrl.newPost
import retrofit2.http.*

interface ApiService{

    @POST(login)
    suspend fun login (@Query("fbid")fbid:String,
                       @Query("name")name:String,
                       @Query("email")email:String,
                       @Query("fcmtoken")fcmtoken:String
    ):UserInfo
    @POST( readUrl)
    suspend fun read (@Query("user")user:String,
                       @Query("kind")kind:String,
                       @Query("kid")kid:String
    ):String
    @POST( insertMessage)
    suspend fun insertMessage (@Query("user")user:String,
                            @Query("kind")kind:String,
                            @Query("kid")kid:String,
                            @Query("content")content:String
    ):List<GetMessage>
    @POST( clickHeart)
    suspend fun clickHeart (@Query("kind")kind:String,
                            @Query("kid")kid:String,
                            @Query("user")user:String
    ):String
    @POST( getMessage)
    suspend fun getMessage (@Query("kind")kind:String,
                                 @Query("kid")kid:String
    ):List<GetMessage>
    @POST( getNewsListUrl)
    suspend fun getNewsList (@Query("user")user:String
    ):List<CommomListModel>
    @POST( getHostPostListUrl)
    suspend fun getHostPostList (@Query("user")user:String
    ):List<GetHostPostList>
    @POST( getFanPostListUrl)
    suspend fun getFansPostList (@Query("user")user:String
    ):List<GetFansPostList>
    @POST( getFashionList)
    suspend fun getFashionList (@Query("user")user:String
    ):List<CommomListModel>
    @FormUrlEncoded
    @POST( newPost)
    suspend fun newPost (@Query("user")user:String,
                         @Query("content")content:String,
                         @Query("link")link:String,
                         @Query("videoLink")videoLink:String,
                         @Field("photo")photo:String
    ):String

    @GET( getAllListUrl)
    suspend fun getAllList (): GetAllList
    @GET( getAdsListUrl)
    suspend fun getAdsList ():List<GetAdsList>
    @GET( getAnnouncementListUrl)
    suspend fun getAnnouncementList ():List<GetAnnounceList>
    @GET( getHotPosts)
    suspend fun getHotPosts ():List<ArticleModel>
}