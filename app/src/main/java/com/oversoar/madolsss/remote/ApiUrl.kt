package com.oversoar.madolsss.remote

object ApiUrl {
    const val hostUrl = "https://puyue.com.tw/madolsss/public/api/"
    const val login = "login"
    const val readUrl = "click"
    const val insertMessage = "insertMessage"
    const val clickHeart = "clickHeart"
    const val getAllListUrl = "alllist"
    const val getAdsListUrl = "adslist"
    const val getAnnouncementListUrl = "announcelist"
    const val getFanPostListUrl = "fanspostlist"
    const val getHostPostListUrl = "articleslist"
    const val getNewsListUrl = "newslist"
    const val getMessage = "getMessage"
    const val newPost = "newPost"
    const val getHotPosts = "getHotPosts"
    const val getFashionList = "getFashionList"
}